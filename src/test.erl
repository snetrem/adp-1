%%%-------------------------------------------------------------------
%%% @author Milan
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Sep 2018 19:41
%%%-------------------------------------------------------------------
-module(test).
-author("Milan").

%% API
-export([insort/2, insortList/2, delete/2, equal/2, find/2, toList/2, empty/1]).

empty({}) ->
  true;

empty(_Btree) ->
  false.

insort({}, X) ->
  {X, {}, {}};

insort({Value, Btree_l, Btree_r}, X) ->
  if
    X == Value -> {Value, Btree_l, Btree_r};
    X < Value ->{Value, insort(Btree_l, X), Btree_r};
    X > Value -> {Value, Btree_l, insort(Btree_r, X)}
  end.

insortList(Btree, []) ->
  Btree;

insortList(Btree, [H | T]) ->
  insortList(insort(Btree, H), T).

delete({}, _X) ->
  {};

delete({Value, Btree_l, Btree_r}, X) ->
  if
    X < Value -> delete(Btree_l, X);
    X > Value -> delete(Btree_r, X);
    X == Value -> replaceElement(Btree_l, Btree_r)
  end.

replaceElement({}, {}) ->
  {};

replaceElement({}, Btree) ->
  Btree;

replaceElement(Btree, {}) ->
  Btree;

replaceElement({Value, Btree_l, Btree_r}, Btree) ->
  {Value, Btree_l, replaceElement(Btree_r, Btree)}.

equal({}, {}) ->
  true;

equal({}, _Btree) ->
  false;

equal(_Btree, {}) ->
  false;

equal({Value_1, Btree_1_l, Btree_1_r}, {Value_2, Btree_2_l, Btree_2_r}) ->
  if
    Value_1 == Value_2 -> equal(Btree_1_l, Btree_2_l) and equal(Btree_1_r, Btree_2_r);
    true -> true
  end.

find({}, _X) ->
  0;

find({Value, Btree_l, Btree_r}, X) ->
  if
    X == Value -> 1;
    X < Value -> find(Btree_l, X);
    X > Value -> find(Btree_r, X)
  end.

toList({}, List) ->
  List;

toList({Value, Btree_l, Btree_r}, List) ->
  List_r = [Value | toList(Btree_r, List)],
  toList(Btree_l, List_r).

